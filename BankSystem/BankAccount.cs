using System;

namespace BankSystem
{
    public class BankAccount
    {
        public const string NEGETIVE_BALANCE_MESSAGE = "Balance is negetive";
        public const string NEGETIVE_AMOUNT_MESSAGE = "Debit/Credit amount is negetive";
        public const string AMOUNT_GREATER_THAN_BALANCE_MESSAGE = "Debit amount is greater than current balance";
        private readonly string username;
        private double m_balance;

        public BankAccount() : this("NaN", 0.00){}
        public BankAccount(string username, double balance)
        {
            this.username = username;
            Balance = balance;
        }

        public string Name{
            get{
                return username;
            }
        }

        public double Balance{
            get{
                return m_balance;
            }
            set{
                if(value < 0) throw new ArgumentOutOfRangeException("value", value, NEGETIVE_BALANCE_MESSAGE);
                else m_balance = value;
            }
        }

        public void Debit(double amount){
            if(amount < 0){
                throw new ArgumentOutOfRangeException("amount", amount, NEGETIVE_AMOUNT_MESSAGE);
            }
            else if(amount > Balance){
                throw new ArgumentOutOfRangeException("amount", amount, AMOUNT_GREATER_THAN_BALANCE_MESSAGE);
            }
            else Balance -= amount;
        }

        public void Credit(double amount){
            if(amount < 0){
                throw new ArgumentOutOfRangeException("amount", amount, NEGETIVE_AMOUNT_MESSAGE);
            }
            else Balance += amount;
        }
    }
}