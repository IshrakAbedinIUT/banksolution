using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using BankSystem;

namespace BankSystemTest
{
    [TestClass]
    public class BankAccountTest
    {
        [TestMethod]
        public void Constructor_WithNegetiveBalance_ShouldThrowArgumentOutOfRangeException(){
            // Arrange
            string username = "Rizvi";
            double initialBalance = -11.11;

            // Act and Assert
            try
            {
                BankAccount bankAccountUnderTest = new BankAccount(username, initialBalance);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message, BankAccount.NEGETIVE_BALANCE_MESSAGE);
                return;
            }
            Assert.Fail("Did not throw expected exception");
        }

        [TestMethod]
        public void Debit_WithNegetiveAmount_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            string username = "Rizvi";
            double initialBalance = 100.11;
            double debitAmount = -11;
            BankAccount bankAccountUnderTest = new BankAccount(username, initialBalance);

            // Act and Assert
            //Assert.ThrowsException<ArgumentOutOfRangeException>(() => bankAccountUnderTest.Debit(debitAmount));

            try
            {
                bankAccountUnderTest.Debit(debitAmount);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message, BankAccount.NEGETIVE_AMOUNT_MESSAGE);
                return;
            }
            Assert.Fail("Did not throw expected exception");
        }

        [TestMethod]
        public void Debit_WithAmountGreaterThanBalance_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            string username = "Rizvi";
            double initialBalance = 100.11;
            double debitAmount = 2000.12;
            BankAccount bankAccountUnderTest = new BankAccount(username, initialBalance);

            // Act and Assert
            //Assert.ThrowsException<ArgumentOutOfRangeException>(() => bankAccountUnderTest.Debit(debitAmount));

            try
            {
                bankAccountUnderTest.Debit(debitAmount);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message, BankAccount.AMOUNT_GREATER_THAN_BALANCE_MESSAGE);
                return;
            }
            Assert.Fail("Did not throw expected exception");
        }

        [TestMethod]
        public void Debit_WithCorrectAmount_ShouldUpdateBalance()
        {
            // Arrange
            string username = "Rizvi";
            double initialBalance = 100.11;
            double debitAmount = 10.11;
            BankAccount bankAccountUnderTest = new BankAccount(username, initialBalance);

            // Act
            bankAccountUnderTest.Debit(debitAmount);
            double expectedAmount = initialBalance - debitAmount;
            double actualAmount = bankAccountUnderTest.Balance;

            // Assert
            Assert.AreEqual(expectedAmount, actualAmount, 0.001, "Did not debit amount properly.");
        }

        [TestMethod]
        public void Credit_WithNegetiveAmount_ShouldThrowArgumentOutOfRangeException()
        {
            // Arrange
            string username = "Rizvi";
            double initialBalance = 100.11;
            double creditAmount = -11;
            BankAccount bankAccountUnderTest = new BankAccount(username, initialBalance);

            // Act and Assert
            //Assert.ThrowsException<ArgumentOutOfRangeException>(() => bankAccountUnderTest.Debit(debitAmount));

            try
            {
                bankAccountUnderTest.Credit(creditAmount);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message, BankAccount.NEGETIVE_AMOUNT_MESSAGE);
                return;
            }
            Assert.Fail("Did not throw expected exception");
        }

        [TestMethod]
        public void Credit_WithCorrectAmount_ShouldUpdateBalance()
        {
            // Arrange
            string username = "Rizvi";
            double initialBalance = 100.11;
            double creditAmount = 10.11;
            BankAccount bankAccountUnderTest = new BankAccount(username, initialBalance);

            // Act
            bankAccountUnderTest.Credit(creditAmount);
            double expectedAmount = initialBalance + creditAmount;
            double actualAmount = bankAccountUnderTest.Balance;

            // Assert
            Assert.AreEqual(expectedAmount, actualAmount, 0.001, "Did not credit amount properly.");
        }
    }
}
